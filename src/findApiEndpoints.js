import glob from 'glob';
import countBy from 'lodash/countBy.js';

const methods = ['GET', 'POST', 'DELETE', 'PUT', 'PATCH'];

export async function findApiEndpoints() {
  console.log('\nFinding api endpoints...\n---');

  const endpoints = (await Promise.all(methods.map((method) => findApiEndpointsForMethod(method))))
    .filter((it) => it.length > 0)
    .flat();

  const endpointCountsByAuthStatus = countBy(endpoints, (it) =>
    it.options.auth ? 'authenticated' : 'open'
  );

  const endpointCountsHumanised = Object.entries(endpointCountsByAuthStatus)
    .map(([k, v]) => `${v} ${k}`)
    .join(', ');

  console.log(`---\nFound ${endpoints.length} endpoints (${endpointCountsHumanised})\n`);

  return endpoints;
}

async function findApiEndpointsForMethod(method) {
  return Promise.all(
    glob.sync(`**/${method}.js`, {cwd: 'src/api'}).map(async (path) => {
      const module = await import(`../../../src/api/${path}`);
      return buildEndpointConfig(
        method,
        '/' + path.substring(0, path.lastIndexOf('/')),
        module.default,
        module.options || {}
      );
    })
  );
}

function buildEndpointConfig(method, path, handler, options) {
  console.log(options.auth === false ? '❗️' : '🔒', method.padEnd(6), path);

  return {
    method,
    handler: (request) => {
      const credentials = request.auth.credentials || {};
      return handler(request, credentials.uid || null);
    },
    path,
    options: {
      auth: 'firebase',
      tags: ['api'],
      ...options
    }
  };
}
