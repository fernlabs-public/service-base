const admin = require('firebase-admin');

module.exports = function () {
  let credential;

  if (process.env.FIREBASE_KEY) {
    try {
      const key = require(process.env.PWD + '/' + process.env.FIREBASE_KEY);
      credential = admin.credential.cert(key);
    } catch (e) {
      if (e.code === 'MODULE_NOT_FOUND') {
        console.error(`Error: Specified firebase key '${process.env.FIREBASE_KEY}' not found.`);
      } else {
        console.error(e);
      }

      process.exit(0);
    }
  } else {
    // Default to credential location set in the GOOGLE_APPLICATION_CREDENTIALS env variable
    credential = admin.credential.applicationDefault();
  }

  admin.initializeApp({credential});

  return admin;
};
