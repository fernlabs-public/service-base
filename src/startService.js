import Hapi from '@hapi/hapi';
import HapiSwagger from 'hapi-swagger';
import HapiInert from '@hapi/inert';
import HapiVision from '@hapi/vision';
import HapiRateLimit from 'hapi-rate-limit';
import HapiFirebaseAuth from 'hapi-firebase-auth';
import HapiPino from 'hapi-pino';
import HapiDevErrors from 'hapi-dev-errors';

import firebase from './connections/firebase.cjs';
import {findApiEndpoints} from './findApiEndpoints.js';
import packageJson from './packageJson.cjs';

const {name, version} = packageJson;

const disableAuth = process.argv.includes('local-mode');

console.log(process.argv);

export function startService() {
  (async () => {
    process.on('unhandledRejection', (err) => {
      console.error(err);
    });

    const server = Hapi.server({
      port: process.env.PORT,
      host: process.env.HOST || '0.0.0.0'
    });

    if (!disableAuth) {
      await server.register({
        plugin: HapiFirebaseAuth
      });
    } else {
      server.auth.scheme('firebase', () => ({
        authenticate: (request, h) => {
          console.error('[WARN] Handling request with local authentication mode');
          return h.authenticated({
            credentials: {
              uid: 'local-test-user',
              name: 'Miss Local'
            }
          });
        }
      }));
    }

    await server.register({
      plugin: HapiPino,
      options: {
        prettyPrint: process.env.NODE_ENV !== 'production',
        logPayload: true,
        logEvents: ['request-error'],
        redact: ['req.headers.authorization']
      }
    });

    await server.register({
      plugin: HapiDevErrors,
      options: {
        showErrors: process.env.NODE_ENV !== 'production'
      }
    });

    await server.register([{plugin: HapiInert}, {plugin: HapiVision}]);

    await server.register([
      {
        plugin: HapiSwagger,
        options: {
          info: {
            title: name,
            version: version
          },
          swaggerUI: true,
          documentationPage: true
        }
      }
    ]);

    await server.register({
      plugin: HapiRateLimit,
      options: {
        pathLimit: 2000,
        userLimit: 80,
        userCache: {
          expiresIn: 60_000 * 4
        }
      }
    });

    server.auth.strategy('firebase', 'firebase', {
      instance: firebase
    });

    const endpoints = await findApiEndpoints(server);
    endpoints.forEach((it) => server.route(it));

    server.route({
      method: 'get',
      handler: (request) => ({
        service: name,
        version,
        credentials: request.auth.credentials
      }),
      path: '/',
      options: {
        auth: false,
        tags: ['api']
      }
    });

    await server.start();

    console.log(`Service ${name}@${version}`);
    console.log(`Running on ${server.info.uri} (Swagger UI at ${server.info.uri}/documentation)\n`);
  })();
}
