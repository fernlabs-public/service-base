import {Client} from '@elastic/elasticsearch';
import {AmazonConnection} from 'aws-elasticsearch-connector';
import {URL} from 'url';

const client = new Client({
  node: {
    url: new URL(process.env.ELASTIC_URL),
    awsConfig: {
      credentials: {
        accessKeyId: process.env.AWS_ID,
        secretAccessKey: process.env.AWS_KEY
      }
    }
  },
  Connection: AmazonConnection
});

export default client;
